# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    LeminDraw.py                                       :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mdelory <mdelory@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/11/23 12:42:09 by mdelory           #+#    #+#              #
#    Updated: 2018/11/23 14:26:53 by mdelory          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #


import sys
import xml.etree.ElementTree


if len(sys.argv) < 3:
    quit()

r = xml.etree.ElementTree.parse(sys.argv[1]).getroot()
l = r[0]
node = filter(lambda n:  n.get('value') != '' and n.get('value') != None, l.findall('mxCell'))
link = filter(lambda n:  n.get('value') == '' and n.get('value') != None, l.findall('mxCell'))
idict = {}

out = open(''.join((sys.argv[1].split('.')[:-1])) + '.lem', 'w')
out.write('#Generated with Love by LeminDraw\n')
out.write('#' + r.get('gridSize') + '\n')
out.write(sys.argv[2] + '\n')

for n in node:
    if n.get('style').find('#d5e8d4') != -1:
        out.write('##start\n')
    if n.get('style').find('#f8cecc') != -1:
        out.write('##end\n')
    value = n.get('value') 
    nid = n.get('id') 
    idict[nid] = value
    x = n[0].get('x')
    y = n[0].get('y')
    out.write(value + ' ' + x + ' ' + y + '\n')

for l in link:
    out.write(idict[l.get('source')] + '-')
    out.write(idict[l.get('target')] + '\n')

out.write('#Generator by mdelory and mmervoye\n')
